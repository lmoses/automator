<?php

class CNotes
{
  public $messages=array();
  public function add($note) {
    $this->messages = array_merge($this->messages,(array) $note);
  }
  public function view() {
    if (!empty($this->messages)) {
      echo "<div class='notes'>\n";
      foreach ($this->messages as $message) {
	if (preg_match('/^([a-z]+):/',$message,$matches)) {
	  echo "<p class='$matches[1]'>$message</p>\n";
        } else {
	  echo "<p>$message</p>\n";
        }
      }
      echo "</div><!-- notes -->\n";
    }
  }
  public function viewstring() {
  $retstr="";
    if (!empty($this->messages)) {
      $retstr.="<div id='notes'>\n";
      foreach ($this->messages as $message) {
	if (preg_match('/^([a-z]+):/',$message,$matches)) {
	  $retstr.="<p class='$matches[1]'>$message</p>\n";
        } else {
	  $retstr.="<p>$message</p>\n";
        }
      }
      $retstr.="</div><!-- notes -->\n";
    }
    return $retstr;
  }
}
