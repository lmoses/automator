<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
  <title><?php echo "$at - $site" ?></title>
  <?php 
   require('style.inc') 
   ?>
   <?php
   require('jquery.inc') 
   ?>
</head>
<body>
 <div class="page">
   <?php require('header.inc') ?>
    <?php $notes->view() ?>
   <?php require('menu.inc') ?>
   <?php require("at_$at.inc") ?>
   <?php require('footer.inc') ?>
 </div>
</body>
</html>
