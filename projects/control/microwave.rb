require 'wifly'
require 'net/telnet'


class Microwave
  def initialize
#    @wifly=WiFly::new("192.168.10.115")    # home
    @wifly=WiFly::new("192.168.7.89") # acm
    @controlPin= 4
    @statusPin= 18
    @wifly.pinModes({@controlPin => :out, @statusPin => :in})
  end

  def set(value)
   # @wifly.digitalWrite(@controlPin,value)

    exec "/var/projects/control/sendDigit !" + value + "%"


 end

  def get
    return @wifly.digitalRead(@statusPin)
  end

  def on(value)

    set(value)
  end

  def off
    set(0)
  end

  def on?
    return (get == 1)
  end

  def off?
    return (get == 0)
  end
end
