require 'wifly'

class Outlet
  def initialize
#    @wifly=WiFly::new("192.168.10.115")    # home
    @wifly=WiFly::new("192.168.7.140") # acm
    @controlPin= 7
    @statusPin= 4
    @wifly.pinModes({@controlPin => :out, @statusPin => :in})
  end

  def set(value)
    @wifly.digitalWrite(@controlPin,value)
  end

  def get
    return @wifly.digitalRead(@statusPin)
  end

  def on
    set(1)
  end

  def off
    set(0)
  end

  def on?
    return (get == 1)
  end

  def off?
    return (get == 0)
  end
end
