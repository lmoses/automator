<?php
// must be logged in
if (!$user->authenticated()) {
  $notes->add("you must login");
  return;
}

// only managers can edit other accounts
$id=isset($_REQUEST['id']) ? $_REQUEST['id'] : $user->id;
if ($user->id != $id && !$user->manager()) {
  $notes->add("not a manager");
  return;
}

$account_user=$user->user_byid($id);

$delete_at = ($user->id == $id) ? "main" : "manage";

echo <<<HTML
<h1>account#$id - $account_user</h1>
<form action='index.php?at=account&pre=user_update' method='post'>
<input type="hidden" name="id" value="$id"/>
<table>
<tr>
  <td>pass:</td>
  <td><input type='password' size='10' name='pass' />
</tr>
<tr>
  <td>again:</td>
  <td><input type='password' size='10' name='verify' />
</tr>
<tr>
  <td colspan='2'><input type='submit' value='update' />
</tr>
</table>
</form>
<form action='index.php?at=$delete_at&pre=user_delete' method='post'>
<input type="hidden" name="id" value="$id"/>
<input type="submit" value="delete"/>   sure? <input type="checkbox" value="yes" name="sure"/>
</form>
HTML;
