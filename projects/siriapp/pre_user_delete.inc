<?php
// must be logged in
if (!$user->authenticated()) {
  $notes->add("you must login");
  return;
}

if (!isset($_REQUEST['id'])) {
  $notes->add("no user id specified");
  return;
}
$id=$_REQUEST['id'];

// only managers can edit other accounts
if ($user->id != $id && !$user->manager()) {
  $notes->add("not a manager");
  return;
}

// cant delete root
if ($id == 1) {
  $notes->add("cannot delete root account");
  return;
}

// sure of operation
if (!isset($_REQUEST["sure"]) || strcmp($_REQUEST["sure"],"yes")!=0) {
  $notes->add("not sure");
  return;
}

// go to manage, or main page?
if ($user->id == $id) {
  $at="main";
} else {
  $at=$user->manager() ? "manage" : "main";
}

$user->delete($id);
