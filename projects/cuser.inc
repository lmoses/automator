<?php

class CUser
{
  public $id;
  public $user;
  public $nonce;
  
  public function __construct($vals = array())
  {
    $this->id=isset($vals['id']) ? $vals['id'] : 0;
    $this->user=isset($vals['user']) ? $vals['user'] : 'anonymous';
    $this->nonce=isset($vals['nonce']) ? $vals['nonce'] :base64_encode(mcrypt_create_iv(16));
  }
  
  public function authenticate($user,$pass)
  {
    global $db;
    global $notes;

    $sql=$db->prepare('SELECT id,user,hash,salt FROM user WHERE user=:user');
    $sql->execute(array(':user'=>$user));

    while ($row=$sql->fetch()) {
      if (strcmp(sha1($user . '/' . trim($pass) . '/' . $row['salt']),$row['hash']) == 0) {
        $_SESSION['user']=array('user'=>$user, 'id'=>$row['id'], 'nonce'=>$this->nonce);
	$this->user=$user;
	$this->id=$row['id'];
	return true;
      }
    }
    $notes->add("error: invalid user or pass");
    return false;
  }

  public static function create($user,$pass)
  {
    global $db;
    global $notes;

    if (!preg_match('/^[_a-z0-9]{3,80}$/',$user)) {
      $notes->add("error: invalid user");
      return false;
    }

    $sql=$db->prepare('SELECT id FROM user WHERE user=:user');
    $sql->execute(array(':user'=>$user));
    if ($row=$sql->fetch()) {
      $notes->add("error: user already exists");
      return false;
    }

    $salt=sha1(mcrypt_create_iv(40));
    $hash=sha1($user . '/' . trim($pass) . '/' . $salt);
    
    $sql=$db->prepare('INSERT INTO user (user,hash,salt) VALUES (:user,:hash,:salt)');
    if ($sql->execute(array(':user'=>$user,':hash'=>$hash,':salt'=>$salt))) {
      $notes->add("added user $user");
    } else {
      $notes->add("failed to add user $user");
    }
  }

  public function user_byid($id)
  {
    global $db;
    global $notes;

    $sql=$db->prepare('SELECT user FROM user WHERE id=:id');
    $sql->execute(array(':id'=>$id));
    if (!$row=$sql->fetch()) {
      $notes->add("no such user id");
      return NULL;
    } else {
      return $row['user'];
    }
  }

  public function update($id,$pass)
  {
    global $db;
    global $notes;

    $sql=$db->prepare('SELECT user,hash,salt FROM user WHERE id=:id');
    $sql->execute(array(':id'=>$id));
    if ($row=$sql->fetch()) {
      $hash=sha1($row['user'] . '/' . trim($pass) . '/' . $row['salt']);
      
      $sql=$db->prepare('UPDATE user SET hash=:hash WHERE id=:id');
      $sql->execute(array(':id'=>$id,':hash'=>$hash));
      $notes->add("password changed");
      return true;
    } else {
      $notes->add("no such user id");
    }
  }

  public function logout()
  {
    session_unset();
    session_destroy();
    $_SESSION = array();
    $this->user='anonymous';
    $this->id=0;
  }

  public function delete($id)
  {
    global $db;
    global $notes;

    $sql=$db->prepare('DELETE FROM user WHERE id=:id');
    $sql->execute(array(':id'=>$id));
    if ($sql->rowCount() > 0) {
      $notes->add("deleted user");
    }
    if ($this->id == $id) {
      $this->logout();
    }
  }

  public function authenticated()
  {
    return $this->id != 0;
  }

  public function manager()
  {
    return $this->id == 1;
  }
}
