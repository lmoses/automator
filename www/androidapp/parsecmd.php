<?php

#command light
$message=strtolower($message);
$verb="";
$noun="";
$pattern="/((turn)?.*(on|off|of).*(light|wall|porch|outlet)|(turn)?.*(light|wall|porch|outlet).*(on|off|of))/i";
$pattern_microwave="/(set)?.*(microwave).*([0-9] (minute[s]?)|[0-9] (second[s]?))/i";
$pattern_check="/check.*(light|porch)/i";
$pattern_check_door="/check.*(window|door)/i";
if(preg_match($pattern, $message, $matches)){
	if(preg_match("/(on|off|of).*(light|wall|porch|outlet)/i",$matches[0],$newMatches))
	{
		$verb=$newMatches[1];
		$noun=$newMatches[2];
		//print_r("verb: ".$verb);
		//print_r("\nnoun: ".$noun);
	} else if(preg_match("/(light|wall|porch|outlet).*(on|off|of)/i",$matches[0],$newMatches))
	{
		$verb=$newMatches[2];
		$noun=$newMatches[1];
		//print_r("verb: ".$verb);
		//print_r("\nnoun: ".$noun);	
	}
}else if(preg_match($pattern_check, $message, $newMatches)){
      //print_r("verb: ".$newMatches[1]);
      //print_r("noun: on?");
      $noun=$newMatches[1];
      //$verb="on?";
      $noun=ucfirst($noun);
      $output='/var/projects/control/ctrl '.$noun.' on?';
      $lightStatus=shell_exec($output);
      $lightStatus=trim($lightStatus);      
      //print_r($lightStatus);
       
     if($lightStatus=="true"){
     $new_str="The ".$noun." is on";
       echo $new_str;
       }else {
       $new_str="The ".$noun." is off";
       echo $new_str;
}
}else if(preg_match($pattern_check_door, $message, $newMatches)){
      //print_r("verb: ".$newMatches[1]);
      //print_r("noun: on?");
      $noun=$newMatches[1];
      //$verb="on?";
      $noun=ucfirst($noun);
      $output='/var/projects/control/ctrl '.$noun.' locked';
      $lightStatus=shell_exec($output);
      $lightStatus=trim($lightStatus);
     // print_r($lightStatus);
      
     if($lightStatus=="true"){
     $new_str="The ".$noun." is Locked";
       echo $new_str;
      }else{
      $new_str="The ".$noun." is Open";
       echo $new_str;
}
}else if(preg_match($pattern_microwave, $message, $matches)){
      //print_r($matches[0]);
      $minutes=0;
      $seconds=0;
      if(preg_match("/[0-9]?[0-9]? (minute[s]?)/i",$matches[0],$minMatches)){
                $minutes=$minMatches[0];
                preg_match("/[0-9]?[0-9]?/i",$minutes,$minMatches);
                $minutes=$minMatches[0];
                //print_r($minutes);
      }
      if(preg_match("/[0-9]?[0-9]? (second[s]?)/i",$matches[0],$secMatches)){
                $seconds=$secMatches[0];
                preg_match("/[0-9]?[0-9]?/i",$seconds,$secMatches);
                $seconds=$secMatches[0];
                //print_r($seconds);
      }
      $time=($minutes*100)+$seconds;
//      print_r("\n" . $time);
      $output= '/var/projects/control/ctrl Microwave on ' . $time;

      print_r(" \n " . $output . " \n ");
      $newexec=system($output . " 2>&1", $retval);
      sleep(3);

}else{
	print_r("no match");
}


if($noun != "" && $verb != ""){
	 $noun=ucfirst($noun);
	 if($verb=="of") {
	 	       $verb="off";
	 }
	 $output= '/var/projects/control/ctrl ' . $noun . " " . $verb;
//	 print_r($output);
	 try {
	//     system($output);
	//     $newexec=shell_exec($output);
	    $newexec=system($output . " 2>&1", $retval);
//	     echo $newexec;
	   //print_r("\n" . $retval);
	     } catch (Exception $e) {
	       echo 'Caught exception: ', $e->getMessage(), "\n";
	     }

}
