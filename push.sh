#!/bin/bash
commit_message=`git status -s`
mkdir /var/projects/bitbucket
cd /var/projects/bitbucket
cp -R /var/www/* /var/projects/bitbucket/www
cp -R /var/projects/siriapp/* /var/projects/bitbucket/projects
cp -R /var/projects/control/* /var/projects/bitbucket/projects
cp -R /home/moses/.rvm/gems/ruby-2.0.0-p0/gems/siriproxy-0.5.4/plugins/siriproxy-example/lib/* /var/projects/bitbucket/plugins


echo $commit_message
git add -A
if [ "$1" != ""]; then
 commit_message=$1
else
 commit_message="The following files have changed: "$commit_message
fi
git commit -m "$commit_message"
git push origin master

