<?php
header("Content-type: text/css");

$cgrey="#404040";
$clblue="#5484be";
$cblue="#274467";
$cdblue="#203855";
$cred="#B71427";
$cyellow="#FFE658";
$cwhite="#FFFFFF";
$ccream="#F5FFFA";
$cdarkgrey="#333";
$cbright="#fbfde0";
$cyellow="#ffe600";
$corange="#ff7e27";
$cred="#450505";
$cblue="#3c689f";
$cblack="#000000";
$cbg="#666";
$cbdr="#888";

$colors=array("p1"=>$cgrey,"p2"=>$cblue,"p3"=>$cdblue,"p4"=>$clblue,"p5"=>"#66A3D2",
	      "fc1"=>$cwhite,"fc2"=>$cyellow,"sa3"=>"#A67C00","sa4"=>"#FFCF40","sa5"=>"#FFDC73",
	      "sb1"=>$cred,"bdr1"=>$cbdr,"bdr2"=>$ccream,"bdr3"=>$cdarkgrey,"sb5"=>"#FF9B73",
              "w0"=>"#FFFFFF","w1"=>"#F0F0F0","w2"=>"#E8E8E8",
	      "b1"=>"#000000","b2"=>"#101010","b3"=>"#181818");

$fonts=array("f1"=>"Arial,Helvetica,sans-serif","h"=>"Helvetica","p"=>"Lucida Grande","tt"=>"Courier");
echo <<<END_OF_FILE

div.menu
{
  background-color: $colors[p1];
  color: $colors[fc1];
  font-family: $fonts[f1];

  border: solid $colors[bdr1] 2px;
  float: left;
  margin-right: 0.5em;
  margin-bottom: 0.5em;
  margin-top: 0.5em;
  border-radius: 0.5em;

}
div.menu h3
{
  text-align: center;
  margin-bottom:0.5em;
  margin-top:0.5em;
}
div.menu a:first-letter
{
    text-transform: capitalize
}
div.menu a.at
{
  background-color:$colors[p4];
} 
div.menu a, a:link, a:visited
{
  color: $colors[fc1];
  background-color: $colors[p2];
  display: block;
  text-decoration: none;
  margin-left:0.5em;
  margin-right:0.5em;
  
  text-decoration: none;
  padding: 0.125em 0.5em 0.125em 0.5em;
  border-top: 1px solid #333333;
  border-right: 1px solid #333333;
  border-bottom: 1px solid #333333;
  border-left: 1px solid #333333;
  border-radius:0.5em;
}
div.menu a:hover
{
  background-color: $colors[p3];
}
.hidden
{
display:none;
}

div.footer
{
  border-top: solid white 1px;
  background-color: $colors[p2];
  color: $colors[fc1];
  font-weight: bold;
  clear: both;
  text-align: right;
  padding-top: 0.5em;
  padding-right: 0.5em;
  padding-bottom: 0.5em;
  border-bottom-right-radius: 0.5em;
  border-bottom-left-radius: 0.5em;

}
.button
{
  text-decoration: none;
  background-color: $colors[p2];
  color: $colors[fc1];
  padding: 0.5em 0.5em 0.5em 0.5em;
  border: 1px solid #333333;
  border-radius:0.5em;
  
}
.button:hover
{
    background-color: $colors[p3];
}
.input
{
  font-family:$fonts[f1];
}
body {
  background-color: $colors[p1];
}

.page {
  margin-left: 2em;
  margin-right: 2em;
  color: $colors[fc1];
  font-family:$fonts[f1];
  background-color: $colors[p1];
  border-radius: 0.5em;
}

div.header {
  padding: 0.5em;
  background-color: $colors[p2];
  color: $colors[fc1];
  font-weight: bold;
  border-top-right-radius: 0.5em;
  border-top-left-radius: 0.5em;
}

.notes
{
  border: solid $colors[bdr1] 2px;
  border-radius: 0.5em;
  background-color: $colors[p2];
  color: $colors[fc1];
  float: right;
  max-width: 10em;
  margin: 0.5em;
  padding: 0.5em;
}
.notes p
{
margin: 0.25em;
}

div.header h1 {
 font-size: 175%;
}
div.header h1:first-letter
{
    text-transform: capitalize
} 

.login,.logout
{
  border: solid $colors[bdr2] 1px;
  border-radius:0.5em;
  background-color: $colors[p1];
  color: white;
  margin: 0em;
  margin-top:0.5em;
  padding: 0.125em;
  text-align: right;
}

.login td
{
  padding: 0.125em;
}
.light_buttons
{
  float: left;
  margin: 0.5em;
}
.main_container
{
  width: 75%;
  float:left;
}
.light_control
{ 
  float:left;
clear: both;
}
.light_on, .light_off
{
  margin: 0.5em;
  float: left;
}

.fourohfourpic
{
width: 80%;
height: 80%;
}


table.manage
{
  border-collapse:collapse;
}

table.manage, table.manage > tbody > tr > th, table.manage > tbody > tr > td
{
  border: solid #fbfde0 1px;
  border-radius: 0.5em;
  vertical-align: top;
  padding: 0.5em;
}

table.manage > tbody > tr > th
{
  font-weight: bold;
}

.manage .group
{
  text-align: right;
  vertical-align: top;
}

.manage .user
{
  text-align: left;
  vertical-align: top;
}

.manage .user
{
  
}

.manage form
{
  border: solid #fbfde0 1px;
  border-radius: 0.5em;
  background-color: #EEE;
  margin: 0.25em;
  padding: 0.25em;
}


.button
{
  text-align: right;
}

.clear
{
 clear: both;
}


END_OF_FILE;
