package com.softeng.homeautomation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
 
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources.NotFoundException;
import android.net.wifi.WifiConfiguration.Protocol;
import android.os.Bundle;
import android.os.Looper;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import org.apache.commons.*;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import java.util.Locale;


public class HomeAutomation extends Activity implements OnInitListener {
	 private static final int VOICE_RECOGNITION_REQUEST_CODE = 10110101;
     private String command="";
	 private Button mbtSpeak;
	 private TextToSpeech tts;
	 private static Context context;
	 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	// Load the self-signed server certificate

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_automation);
        mbtSpeak = (Button) findViewById(R.id.btSpeak);
        tts = new TextToSpeech(this, this);
        
        checkVoiceRecognition();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_automation, menu);
        return true;
    }
    public void checkVoiceRecognition() {
    	
    	  // Check if voice recognition is present
    	  PackageManager pm = getPackageManager();
    	  List<ResolveInfo> activities = pm.queryIntentActivities(new Intent(
    	    RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
    	  if (activities.size() == 0) {
    	   mbtSpeak.setEnabled(false);
    	   mbtSpeak.setText("Voice recognizer not present");
    	   Toast.makeText(this, "Voice recognizer not present",
    	     Toast.LENGTH_SHORT).show();
    	  }
    	 }
    	 
    	 public void speak(View view) {
    	  Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
    	  startActivityForResult(intent, VOICE_RECOGNITION_REQUEST_CODE);
    	 }
    	 
    	 @Override
    	 protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	  if (requestCode == VOICE_RECOGNITION_REQUEST_CODE)
    	   if(resultCode == RESULT_OK) {
    	    ArrayList<String> textMatchList = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
    	    if (!textMatchList.isEmpty()) {
    	    	command=textMatchList.get(0);
    	    	showToastMessage(command);
    	    	sendToServer(command);	
//    	    	speakOut("Attempting Command: "+command);
    	    }
    	   //Result code for various error.
    	   }else if(resultCode == RecognizerIntent.RESULT_AUDIO_ERROR){
    	    showToastMessage("Audio Error");
    	   }else if(resultCode == RecognizerIntent.RESULT_CLIENT_ERROR){
    	    showToastMessage("Client Error");
    	   }else if(resultCode == RecognizerIntent.RESULT_NETWORK_ERROR){
    	    showToastMessage("Network Error");
    	   }else if(resultCode == RecognizerIntent.RESULT_NO_MATCH){
    	    showToastMessage("No Match");
    	   }else if(resultCode == RecognizerIntent.RESULT_SERVER_ERROR){
    	    showToastMessage("Server Error");
    	   }
    	  super.onActivityResult(requestCode, resultCode, data);
    	 }
    	 
    	 /////////// HTTPS to server
//    	  public void sendToServer(String command){
//    		// Instantiate the custom HttpClient
//    		  String newCommand=URLEncoder.encode(command);
//    		  
//    		  String myURL="http://192.168.7.181/post.php?message="+newCommand;
////    		  String myURL="https://192.168.7.181/post.php?message="+newCommand;
//
//    		  DefaultHttpClient client = new MyHttpClient(getApplicationContext());
//    		  HttpGet get = new HttpGet(myURL);
//    		  try {
//				HttpResponse response = client.execute(get);
//				showToastMessage(response.toString());
//			} catch (ClientProtocolException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//    		  
//    	  }
    	  public void sendToServer(final String command) {
    		  new Thread(new Runnable() {
    		    public void run() {
    		    	Looper.prepare();
    		    	String newCommand=URLEncoder.encode(command);
    	    		  
//    	    		  String myURL="http://192.168.7.88/androidapp/post.php?message="+newCommand;
    	    		  String myURL="http://192.168.7.88/androidapp/post.php?message="+newCommand;
    	    		  
    	
    	    		  DefaultHttpClient client = new MyHttpClient(getApplicationContext());
    	    		  HttpGet get = new HttpGet(myURL);
    	    		  try {
    					HttpResponse response = client.execute(get);
//    					showToastMessage(response.toString());
    						if(response!=null)
    			        	{
    							   BufferedReader in = new BufferedReader (new InputStreamReader(response.getEntity().getContent()));
    						        StringBuffer sb = new StringBuffer("");
    						        String line = "";
    						        String NL = System.getProperty("line.separator");
    						        while ((line = in.readLine()) != null) {
    						            sb.append(line + NL);
    						        }
    						        in.close();
    						        String page = sb.toString();
    						        page = page.trim();
//    						        page=page.contentEquals(strbuf)
    						       // showToastMessage(page + " = Success");
    						        //speakOut(page);
    						        if (page.length()<25){
    						        	speakOut(page);
    						        }else {
    						        	speakOut("There was an error"); 
    						        }
//    						        if(page.equals("Success")){
////    						        	showToastMessage(page);
//    						        	speakOut(page);
//    						        } else {
//    						        	//showToastMessage("error");
//    						        	speakOut("I could not run your command. There was an error!");
//    						        }
    			        	} else {
    			        		speakOut("No Connection to Server");
    			        	}
    				} catch (ClientProtocolException e) {
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				} catch (IOException e) {
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				}
    	    		  Looper.loop();
    		    }
    		    
    		  }).start();
    		}
    	  
    	  
    	  
    	 void showToastMessage(String message){
    	  Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    	 }


		@Override
		public void onInit(int status) {
		       if (status == TextToSpeech.SUCCESS) {
		    	   
		            int result = tts.setLanguage(Locale.US);
		 
		            if (result == TextToSpeech.LANG_MISSING_DATA
		                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
		                Log.e("TTS", "This Language is not supported");
		            } else {
		                speakOut("Ready for command");
		            }
		 
		        } else {
		            Log.e("TTS", "Initilization Failed!");
		        }
			
		}
	    private void speakOut(String text) {
	    	 
	        //String text = "Hello, please enter a command";
	 
	        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
	    }

}

















