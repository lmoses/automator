<?php
// must be logged in
if (!$user->authenticated()) {
  $notes->add("you must login");
  return;
}

if (!isset($_REQUEST['id'])) {
  $notes->add("no user id specified");
  return;
}
$id=$_REQUEST['id'];

if (!$_REQUEST['pass'] || !trim($_REQUEST['pass'])) {
  $notes->add("no password specified");
  return;
}

if (!$_REQUEST['verify'] || (strcmp(trim($_REQUSET['pass']),trim($_REQUSET['pass']))!=0)) {
  $notes->add("password not verified");
  return;
}

// only managers can edit other accounts
if ($user->id != $id && !$user->manager()) {
  $notes->add("not a manager");
  return;
}

$user->update($id,trim($_REQUEST['pass']));
