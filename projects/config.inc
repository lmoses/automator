<?php
  //site name
$site="Automator";

// start/allow session variables
session_start();

//connect to db
try {
  $db = new PDO("mysql:host=localhost;dbname=siriapp","siriapp","thjN6TyRc4n462ZY");
} catch (Exception $e) {
  echo "PDO connection error: " . $e->getmessage();
  exit(1);
  }

// setup class auto-loader                                                                          
function classloader($class) {
  require_once(strtolower($class).".inc");
}
spl_autoload_register('classloader');


// objects we almost always want
$notes=new CNotes();
$user=new CUser(isset($_SESSION['user']) ? $_SESSION['user'] : array());


//decide what page we are at
$at="main"; //default
if (isset($_REQUEST["at"])) $at=$_REQUEST["at"];
if (! (preg_match('/^[-_a-z0-9]*$/',$at) && file_exists("at_$at.inc"))) {
  $at="404";
}

// apply all pre-load steps                                                                         
if (isset($_REQUEST['pre'])) {
  foreach (explode(',',$_REQUEST['pre']) as $pre) {
    if (preg_match("/^[-_a-z0-9]*$/",$pre) && file_exists("pre_$pre.inc")) {
      include("pre_$pre.inc");
    } else {
      $notes->add('error: missing preload');
    }
  }
}

