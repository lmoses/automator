<?php
// setup menu items
$menu=array("main");

if ($user->authenticated()) {
  array_push($menu,"account");
#  array_push($menu,"devices");
}

if ($user->manager()) {
  array_push($menu,"manage");
}

?>

<div class="menu">
<h3>Menu</h3>
<?php
  foreach ($menu as $item) {
    $class=($at == $item) ? "class='at'" : "";
    echo "<a $class href='index.php?at=$item'>$item</a>\n";
  }
?>
<?php 
  if ($user->authenticated()) {
    require('logout.inc');
  } else {
    require('login.inc');
  }
?>
</div><!-- menu -->
