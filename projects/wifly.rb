require 'net/telnet'

#  === WiFly 171-XV remote wireless I/O Module ===
#
#  This is a simplified interface to use this module for digital i/o
#  and analog input.  It assumes the module has been configured and
#  is accessible via a host name or IPv4 address.
#
#  Example use:
#
#  require('wifly')
#
#  nightlightAddress = "nightlight.myhome.net"
#
#  relayPin = 4       # to relay controlling light
#  statusPin = 18     # from relay for status
#  brightnessPin = 20 # brightness (analog) sensor
#
#  tooBright = 1000
#  tooDim = 750
#
#  nightlight = WiFly::new(nightlightAddress)
#
#  nightlight.pinMode({switchPin => :out, 
#                      statusPin => :in, 
#                      brightnessPin => :sensor})
#
#  if nightlight.digitalRead(statusPin) == 1 then
#    if nightlight.analogRead(brightnessPin) > tooBright then
#      nightlight.digitalWrite(relayPin,0)
#    end
#  else
#    if nightlight.analogRead(brightnessPin) < tooDim then
#      nightlight.digitalWrite(relayPin,1)
#    end
#  end
#
#  === Hardware Quick Reference ===
#
#  EXCEPT the Analog/Sens lines, this board is 3.3V
#  Pins configured for analog are ONLY 1.2V tolerant.
#
#
#     /-----------\
#    /             \
#   / WiFly 171-XV  \
#  /                 \
#  |[1]VDD      A2 20|
#  | 2 TX    d2/A3 19|
#  | 3 RX    d3/A4 18|
#  | 4 D8       A5 17|
#  | 5 RESET   CTS 16|
#  | 6 D5    D6/A7 15|
#  | 7 D7   unused 14|
#  | 8 ADHOC D4/A6 13|
#  | 9 d1      RTS 12|
#  |10 GND     d14 11|
#  +-----------------+
#
#  [1] this is a square pad
#   D - 24mA GPIO
#   d -  8mA GPIO
#   A - 1.2V tolerant 0-400 mV 5% accurate ADC.
#
#Pin   Name            Desc
#  1   VDD             power (square pad)
#  2   UART TX         8mA output
#  3   UART RX         input
#  4   GPIO D8         24mA i/o
#  5   RESET           100k ohm pullup, low for >160uS to reset
#  6   GPIO D5         24mA i/o (optional data tx/rx)
#  7   GPIO D7         24mA i/o
#  8   AD HOC          Pull high to enable ad-hoc mode
#  9   GPIO d1         8mA i/o
# 10   GND             ground
# 11   GPIO d14        8mA i/o
# 12   UART RTS        Flow control 8mA (out)
# 13   GPIO D4/SEN A6  24mA i/o or 0-400mV ADC (default GPIO)
# 14   Not Used
# 15   GPIO D6/SEN A7  24mA i/o or 0-400mV ADC (default GPIO)
# 16   UART CTS        CTS (in)
# 17   SEN A5          0-400mV ADC
# 18   GPIO D3/SEN A4  8mA i/o or 0-400 ADC (default GPIO)
# 19   GPIO D2/SEN A3  8mA i/o or 0-400 ADC (default ADC)
# 20   SEN A2          0-400mV ADC
#
#  === References ===
#  
#  * module 
#    - http://rovingnetworks.com/resources/download/16/RN_XV
#  * reference
#    - http://rovingnetworks.com/resources/download/93/WiFly_User_Manual
#
###
class WiFly

  @@gpio_masks = {
  #pin => register mask
     4 => (1 << 8), 
     6 => (1 << 5), 
     7 => (1 << 7), 
     8 => (1 << 9),
     9 => (1 << 1),
    11 => (1 << 14),
    15 => (1 << 6),
    18 => (1 << 3),
    19 => (1 << 2) 
  }

  @@sensor_masks = {
  #pin => register mask
    13 => (1 << 6),
    15 => (1 << 7),
    17 => (1 << 5),
    18 => (1 << 4),
    19 => (1 << 3),
    20 => (1 << 2)
  }

#
# Connect to the WiFly using its IP4 address and enter command mode.
#
# If in AD HOC mode (pin 9 is high), then the WiFly creates an ad hoc
# network where it's address is 169.254.1.1 --- this should only happen
# when initially configuring the wifly for your network.
#
  def initialize(host = "169.254.1.1")
    @wifly = Net::Telnet::new({"Host" => host, "Port" => 2000, "Timeout" => 2,"Telnetmode" => false})
    @wifly.puts("$$$")
    sleep(0.250)
    flush
  end
  def sendNewCmd(cmd)
    
    @wifly.puts(cmd)

  end
#
# Set the ssid and password for the network, save the results, and
# reboot.  You will have to determine the IP address of the WiFly
# device by querying the DHCP server after calling this function.
#
# Note: the ssid and phrase cannot have a '$' symbol in them.
#
  def configure(ssid,phrase)
    if ssid =~ /$/ then
      raise 'ssid cannot contain $'
    end
    ssid.gsub!(/ /,'$')

    if phrase =~ /$/ then
      raise 'wpa pass phrase cannot contain $'
    end
    phrase.gsub!(/ /,'$')

    @wifly.puts('set wlan ssid ' + ssid)
    @wifly.puts('set wlan phrase ' + phrase)
    @wifly.puts('set wlan hide 1')
    @wifly.puts('save ' + ssid)
    @wifly.puts('save')
    @wifly.puts('reboot')
  end

  def save(name="")
    if name != "" then
      cmd('save ' + name)
    else
      cmd('save')
    end
  end

  def reboot
    # get ip address
    @wifly 
    @wifly = Net::Telnet::new({"Host" => host, "Port" => 2000, "Timeout" => 1})

    @wifly.puts('reboot')
  end

#
# Send command, and return response.
#
# If flush has not been previously called, the
# response might include output from previous actions
#
  def cmd(message)
    ans = ""
    @wifly.cmd(message) { |part| ans += part }
    return ans
  end

  def flush
    return cmd("")
  end

#
# For example
#
# <pre>
#  wifly.pinModes({ 4 => :out0, 18 => :in, 20 => :sensor })
# </pre>
#
# Note the pins are the pads on the xbee board.  The possible
# pin modes are :in, :out, and :sensor, where the :out0
# and :out1 specify the default boot signals.
#
  def pinModes(pin_modes)
    if cmd("get sys") =~ /IoMask=0x(.*)/
      masks=$1.to_i(16)
    else
      masks=0x21f0
    end
    old_masks = masks
    analogs = []
    pin_modes.each do |pin,mode|
      if mode == :sensor
        analogs.push(pin)
      else
        mask=@@gpio_masks[pin]
        if mode == :out
          masks |= mask
        elsif mode == :in
          mask  &= ~mask
        else
          throw "invalid pin mode"
        end
      end
    end
    if old_masks != masks then
      cmd("set sys mask 0x" + masks.to_s(16) + " 1")
    end
    if analogs then
      analogReads(analogs)
    end
    nil
  end

  def digitalReads(pins)
    ans = {}
    flush
    @wifly.puts("show io")
    ios = @wifly.waitfor(/[0-9a-fA-F]{4,4}/)
    if ios =~ /([0-9a-fA-F]{4,4})/
      io=$1.to_i(16)
      pins.each do |pin|
        ans[pin] = ((io & @@gpio_masks[pin]) != 0) ? 1 : 0;
      end
    end
    return ans
  end

  def digitalRead(pin)
    return digitalReads([pin])[pin];
  end

  #
  # You must set all the output values
  # at once with this --- each call
  # assumes all unset GPIO values are 0
  #
  def defaultValues(pin_values)
    values=0
    pin_values.each do |pin,value|
      mask=@@gpio_masks[pin]
      values |= mask if value != 0
    end
    cmd("set sys value 0x" + values.to_s(16))
    return nil
  end

  def digitalWrites(pin_values)
    masks=0
    values=0
    pin_values.each do |pin,value|
      mask=@@gpio_masks[pin]
      masks |= mask
      values |= mask if value != 0
    end
    cmd("set sys out 0x" + values.to_s(16) + " 0x" + masks.to_s(16) + " 1")
    return nil
  end

  def digitalWrite(pin,value)
    return digitalWrites({pin => value})
  end

  def analogReads(pins)
    mask=0
    unmask = {}
    pins.each do |pin|
      sens_mask = @@sens_mask[pin]
      mask |= sens_mask
      unmask[pin] = sens_mask
    end

    unmask = unmask.sort_by { |pin,mask| mask }

    ans = {}
    flush
    @wifly.puts("show q 0x1" + mask.to_s(16))
    ios = @wifly.waitfor(/(8[0-9]+,)*/)
    count = 0
    while ios =~ /8([0-9]+),/ do
      ans[unmask[count][0]]=$1.to_i 
      ++count
    end
    return ans
  end

  def analogRead(pin)
    return analogReads([pin])[pin]
  end
end
