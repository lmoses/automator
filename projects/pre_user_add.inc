<?php
if (!$user->manager()) {
  $notes->add("not manager");
  return;
}

if (strcmp(trim($_REQUEST['pass']),trim($_REQUEST['verify'])) != 0) {
  $notes->add("password not verified");
  return;
}

$user->create(trim($_REQUEST['user']),trim($_REQUEST['pass']));
